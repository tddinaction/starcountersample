﻿using Starcounter;
using Starcounter.Internal;
using Simplified;

namespace HelloWorldMapper
{
    class Program
    {
        static void Main()
        {
            UriMapping.OntologyMap<Expense>("/HelloWorld/partial/expense/[?]");

            StarcounterEnvironment.RunWithinApplication("Images", () => {
                Handle.GET("images/partials/concept-expense/[?]", (string objectId) => {
                    return Self.GET("images/partials/concept/" + objectId);
                });

                UriMapping.OntologyMap<Expense>("images/partials/concept-expense/[?]");

            });
        }
    }
}