This initial setting for Starcounter are created following the manual from https://starcounter.io/

Starcounter is an in-memory application platform built by Starcounter AB. The platform is based on a combined in-memory database engine and application server.